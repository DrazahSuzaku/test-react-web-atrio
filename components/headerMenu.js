import Link from "next/link";
import { useRouter } from "next/router";

export default function HeaderMenu() {

    const router = useRouter();

    return (
        <header className="bg-black p-4">
            <ul className="flex gap-8">
                <li className="mr-6">
                    <p className="text-white">RobotDemoApp</p>
                </li>
                <li className={router.pathname === "/" ? "mr-6 text-yellow-200" : "text-white mr-6"}>
                    <Link href="/">Accueil</Link>
                </li>
                <li className={router.pathname === "/articles" ? "mr-6 text-yellow-200" : "text-white mr-6"}>
                    <Link href="/articles">Liste des articles</Link>
                </li>
            </ul>
        </header>
    )

}