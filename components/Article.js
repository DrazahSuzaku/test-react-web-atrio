import Link from "next/link";

export default function Article({ post }) {

    return (
        <section className="bg-white rounded-lg p-6">
            <h2 className="text-2xl pb-4">{post.label}</h2>

            <div className="rounded bg-gray-400 p-28 flex justify-center">
                <p className="text-white text-4xl">IMAGE</p>
            </div>

            <p className="text-center text-gray-600 p-4">{post.content}</p>

            <div className="flex justify-center">
                <div className="w-72 p-4 text-xl bg-yellow-400 shadow rounded text-center">
                    <Link href={"/articles/"+post.id}>Découvrir</Link>
                </div>
            </div>
        </section>
    )
}